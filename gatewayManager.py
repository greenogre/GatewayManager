#!/usr/bin/python
# -*- coding: utf-8 -*-

# Import libs
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
 
from time import sleep, time, strftime

import ovpnMgr
try:
    from gpiozero import Button, PWMLED, RGBLED
    hasButton = True
except:
    hasButton = False
    

#global lastState
#global realState
#global nextState
#global buttonLong
#global hasButton
#global buttonRGB
    
buttonRGB = True              # RGB status LEDs? 
buttonLong = 3.0
buttonShort = 1.0

debug_logging = True
debuglog = "/tmp/gwmd.log"
lastlog = 0.0

lastState = 0
realState = 0
nextState = 0
#ledColour = (0, 0, 0)

def logit(logText):
    if debug_logging:
        now = strftime("%c")
        dlog = open(debuglog, 'a')
        dlog.write( "%s: %s\n" % (now,logText) )
        dlog.close()

if hasButton:
    # Init button device
    bigButton = Button(pin=17) #,bounce_time=1,pull_up=True)        # GPIO17 Physical 11
    # buttonLED = PWMLED(18)        # GPIO18 Physical 12 
    # Grnd to button & LEDs       # Physical 14
    
    # LED params
    BLINK_ON = 0.25
    BLINK_OFF = 0.25
    FADE_IN = 0.5
    FADE_OUT = 0.5
    fbOn = (BLINK_ON/2)
    fbOff = (BLINK_OFF/2)
    ffIn = (FADE_IN/2)
    ffOut = (FADE_OUT/2)
    cycleTime = BLINK_ON + BLINK_OFF + FADE_IN + FADE_OUT
    # RGB Colours
    colourUp=(0,0.2,0)
    colourDown=(0.2,0,0)
    colourChange=(0.2,0.33,0)
    colourUnknown=(0.2,0,0.2)
    
    if buttonRGB:
        buttonLED = PWMLED(4)        # GPIO18 Physical 7 
        statusLED = RGBLED(18,22,27) # Physical 12, 15, 13
    else:
        buttonLED = PWMLED(18)        # GPIO18 Physical 12 
        statusLED = PWMLED(27)         # GPIO27 Physical 13
    


def gw_status():
    #print( "Getting status ...")
    result = ovpnMgr.m2mg( 'status' )
    #print( "Result: %d:%s" % (result[0], result[1]) )
    if result[0] == 0:
        if result[1] == 'UP':
            gw_status = 1
        elif result[1] == 'DOWN':
            gw_status = -1
        else:
            gw_status = 0
    else:
        gw_status = 0
    return gw_status

def gw_up():
    #print( "gw_up....")
    result = ovpnMgr.m2mg( 'up' )
    return result

def gw_down():
    #print( "gw_down....")
    result = ovpnMgr.m2mg( 'down' )
    return result

def bothLED_on():
    if not buttonLED.is_lit and not statusLED.is_lit:
        buttonLED.blink(on_time=0, off_time=0, fade_in_time=FADE_IN, fade_out_time=0, n=1, background=True)
        statusLED.blink(on_time=0, off_time=0, fade_in_time=FADE_IN, fade_out_time=0, n=1, background=False)
        buttonLED.on()
        statusLED.on()
    elif statusLED.is_lit:    
        bLED_on()
    elif buttonLED.is_lit:
        sLED_on()

def bothLED_off():
    if buttonLED.is_lit and statusLED.is_lit:
        buttonLED.blink(on_time=0, off_time=0, fade_in_time=0, fade_out_time=FADE_OUT, n=1, background=True)
        statusLED.blink(on_time=0, off_time=0, fade_in_time=0, fade_out_time=FADE_OUT, n=1, background=False)
        buttonLED.off()
        statusLED.off()
    elif statusLED.is_lit:    
        sLED_off()
    elif buttonLED.is_lit:
        bLED_off()

def bLED_on():
    if not buttonLED.is_lit:
        buttonLED.blink(on_time=0, off_time=0,
                        fade_in_time=FADE_IN, fade_out_time=0,
                        n=1, background=False)
        buttonLED.on()
def bLED_off():
    if buttonLED.is_lit:
        buttonLED.blink(on_time=0, off_time=0,
                        fade_in_time=0, fade_out_time=FADE_OUT,
                        n=1, background=False)
        buttonLED.off()
def bLED_blink(blinks=None):
    buttonLED.blink(on_time=BLINK_ON, off_time=BLINK_OFF,
                    fade_in_time=FADE_IN, fade_out_time=FADE_OUT,
                    n=blinks, background=True)
def sLED_on():
    if not statusLED.is_lit:
        statusLED.blink(on_time=0, off_time=0,
                        fade_in_time=FADE_IN, fade_out_time=0,
                        n=1, background=False)
        statusLED.on()
def sLED_off():
    if statusLED.is_lit:
        statusLED.blink(on_time=0, off_time=0,
                        fade_in_time=0, fade_out_time=FADE_OUT,
                        n=1, background=False)
        statusLED.off()
def sLED_blink(blinks=None):
    statusLED.blink(on_time=BLINK_ON, off_time=BLINK_OFF,
                    fade_in_time=FADE_IN, fade_out_time=FADE_OUT,
                    n=blinks, background=True)
    
def setLEDs(ledState):    
    if buttonRGB:
        if ledState == 4:
            #print( "State 4")
            # Gateway UP #Solid green
            if statusLED.color != colourUp:
            #statusLED.blink(on_time=0, off_time=0,
            #                fade_in_time=FADE_IN, fade_out_time=0,
            #                on_color=colourUp, off_color=colourUp,
            #                n=1, background=False)
                #statusLED.off()
                statusLED.color = colourUp
            #ledColour = colourUp
        elif ledState == 3:
            #print( "State 3")
            # Gateway going DOWN # Flash Green/Amber
            statusLED.blink(on_time=BLINK_ON, off_time=BLINK_OFF,
                            fade_in_time=FADE_IN, fade_out_time=FADE_OUT,
                            on_color=colourUp, off_color=(0,0,0), #colourChange,
                            n=3, background=False)
            statusLED.color = colourChange
            #ledColour = colourChange
            #sleep(2 * cycleTime)
        elif ledState == 2:
            #print( "State 2")
            # Gateway going UP # Flash Red/Amber
            statusLED.blink(on_time=BLINK_ON, off_time=BLINK_OFF,
                            fade_in_time=FADE_IN, fade_out_time=FADE_OUT,
                            on_color=colourDown, off_color=(0,0,0), #colourChange,
                            n=3, background=False)
            statusLED.color = colourChange
            #ledColour = colourChange
            #sleep(2 * cycleTime)
        elif ledState == 1:
            #print( "State 1")
            #Gateway DOWN # Solid Red
            if statusLED.color != colourDown:
                statusLED.blink(on_time=0, off_time=0,
                            fade_in_time=FADE_IN, fade_out_time=0,
                            on_color=colourDown, off_color=colourDown,
                            n=1, background=False)
                #statusLED.off()
                statusLED.color = colourDown
            #ledColour = colourDown
        elif ledState == 0:
            #print( "State 0")
            #Gateway unknown state #Slow flash Amber/Off
            statusLED.blink(on_time=BLINK_ON, off_time=BLINK_OFF,
                            fade_in_time=FADE_IN, fade_out_time=FADE_OUT,
                            on_color=colourUnknown, off_color=(0,0,0),
                            n=3, background=False)
            statusLED.color = colourUnknown #( 0, 0.2, 0)
            #ledColour = ( 0, 0.2, 0)
            #sleep(3)
        else:
            #Something broke #Rapid flash Red/Green
            statusLED.blink(on_time=fbOn, off_time=fbOff,
                            fade_in_time=ffIn, fade_out_time=ffOut,
                            on_color=colourUp, off_color=colourDown,
                            n=3, background=False)
            #sleep(2 * cycleTime)
    else:
        if ledState == 4:
            # Gateway UP # bLED on solid
            bLED_on()
            sLED_off()
        elif ledState == 3:
            # Gateway going DOWN # bLED on solid, sLED flash
            bLED_on()
            sLED_blink(3)
            sleep(cycleTime)
        elif ledState == 2:
            # Gateway going UP # bLED off, sLED flash
            bLED_off()
            sLED_blink(3)
            sleep(3)
        elif ledState == 1:
            #Gateway DOWN  # All off
            bothLED_off()
        elif ledState == 0:
            #Gateway unknown state #sLED  flash
            sLED_blink(3)
            sleep(cycleTime)
        else:
            #Something broke  #Rapid flash both
            statusLED.blink(on_time=fbOn, off_time=fbOff,
                            fade_in_time=ffIn, fade_out_time=ffOut,
                            n=3, background=True)
            buttonLED.blink(on_time=fbOn, off_time=fbOff,
                            fade_in_time=ffIn, fade_out_time=ffOut,
                            n=3, background=True)
            sleep(3)
        
        
def button():
    # global lastState
    global realState
    global nextState

    buttonDown = time()
    while True:
        if ( not bigButton.is_pressed ) or ( time() > ( buttonDown + buttonLong ) ):
            buttonUp = time()
            break
        else:
            sleep(0.1)
    
    #print( "button oldState %s" % realState)
    
    if buttonUp >= ( buttonDown + buttonLong ):
        logit( "Long press.")
        #print( "Long press.")
        # lastState = realState
        realState = gw_status()
        if ( realState != 0 ) and ( realState == nextState ):
            #print( "They're equal, system stable, change state" )
            nextState = realState * -1
        else:
            dummyVar = None
            logit("Button long press but system not stable.")
    elif buttonUp >= ( buttonDown + buttonShort ):
        dummyVar = None
        logit("Short press.")
        #print( "Short press.")
    else:
        dummyVar = None
        logit("Not a press.")
        #print( "Not a press.")
    
    #print( "button lastState: %d realState: %d nextState %d" % ( lastState, realState, nextState) )
    
        
def main():
    global lastState
    global realState
    global nextState
    global lastlog
    # Init realState and nextState
    logit( "\n\n============================\n=== GatewayManager init. ===\n============================" )
    realState = gw_status()
    nextState = realState
    logit( "\n============================\n=== Gateway initial state: %s\n============================" % realState)
    #print( "lastState %s" % lastState)
    #print( "curState %s" % realState)
    #print( "nextState %s" % nextState)
    #print( "---")
    
    if realState > 0:
        bLED_on()
    elif realState < 0:
        bothLED_off()
    else:
        bLED_off()
        sLED_blink(1)
    
    # Do something when button pressed
    bigButton.when_pressed = button
    
    while True:
        oldState = realState
        #print( "lastState %s" % lastState)
        #print( "oldState %s" % oldState)
        realState = gw_status()
        if ( realState == 0 ) or \
           ( realState != nextState ) or \
           ( lastState != realState ) or \
           ( oldState != realState ) or \
           ( lastlog + 120.0 ) <= time():
            ledColour = statusLED.color
            logit("lastState:%s oldState:%s curState:%s nextState:%s ledColour:%s" % (lastState,oldState,realState,nextState,ledColour))
            # logit("ledColour: %s" % statusLED.color())
            lastlog = time()
        #print( "curState %s" % realState)
        #print( "nextState %s" % nextState)
        #print( "---")
        if ( realState == 0 ) or ( realState != nextState ):
            if realState != nextState:
                if nextState > 0:
                    gw_up()
                    setLEDs(2)
                elif nextState < 0:
                    setLEDs(3)
                    gw_down()
            else:
                setLEDs(0)
            lastlog = 0.0
        else:
            if realState == nextState:
                lastState = realState
            if realState > 0:
                setLEDs(4)
            elif realState < 0:
                setLEDs(1)
                        
        sleep(2)


if __name__ == "__main__":
    main() 
    
