#!/bin/bash

for i in `seq 1 12`; do
  sleep 5
  
  if [ -e /var/run/openvpn.socket -a -S /var/run/openvpn.socket ]; 
    break
  fi
done

cd /home/drew/GatewayManager

/home/drew/GatewayManager/gatewayManager.py &

cd -
