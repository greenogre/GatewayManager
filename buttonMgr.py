#!/usr/bin/python
# -*- coding: utf-8 -*-

# Import libs
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

try:
    from gpiozero import Button, PWMLED, RGBLED
    rpi = True
except:
    rpi = False
    
import sys
import re
from signal import pause
import time

from datetime import datetime
from pprint import pformat
# from collections import OrderedDict

# Define functions    
BIG_SWITCH = Button(17) #, pull_up=True, bounce_time=BOUNCE_TIME)    # Physical 11
BIG_SWITCH_LED = PWMLED(18)   # Physical 12 
M2MG_LED = PWMLED(27)         # Physical 13
# STATUS_LED = RGBLED(18,27,22) # Physical 12, 13, 15
# Grnd to button & LEDs       # Physical 14

COLOUR_UP=(0,1,0)
COLOUR_DOWN=(1,0,0)
COLOUR_CHANGE=(1,0.5,0)

BLINK_ON = 0.33
BLINK_OFF = 0.33
FADE_IN = 0.5
FADE_OUT = 0.5
BLINKS = 5
CYCLE_TIME = BLINK_ON + BLINK_OFF + FADE_IN + FADE_OUT
SYNC_TIME_ON = ( CYCLE_TIME * BLINKS ) - ( CYCLE_TIME / 2 )
SYNC_TIME_OFF = ( CYCLE_TIME * BLINKS ) - ( CYCLE_TIME / 2 )

def m2mgState():
    print( "Getting gateway state ..." )
    time.sleep(4)
    gwState = True  # Get state from OpenVPN or fping
    if gwState:
        BIG_SWITCH_LED.blink(on_time=0, off_time=0, fade_in_time=FADE_IN, fade_out_time=0, n=1, background=False)
        #BIG_SWITCH_LED.blink(on_time=2, off_time=0, fade_in_time=2, fade_out_time=2, background=True, n=10, )
        BIG_SWITCH_LED.on()
        print( "Gateway is up.\n" )
    else:
        BIG_SWITCH_LED.blink(on_time=0, off_time=0, fade_in_time=0, fade_out_time=FADE_OUT, n=1, background=False)
        BIG_SWITCH_LED.off()
        print( "Gateway is down.\n" )
                  
def button():
    btnDown = time.time()
    longPress = 3.0

    while True:
        if ( not BIG_SWITCH.is_pressed ) or ( time.time() > ( btnDown + longPress ) ):
            btnUp = time.time()
            break
        else:
            time.sleep(0.1)
    
    if btnUp >= ( btnDown + longPress ):
        if BIG_SWITCH_LED.is_lit:
            print( "Long press, taking gateway down ..." )
            M2MG_LED.blink(on_time=BLINK_ON, off_time=BLINK_OFF, fade_in_time=FADE_IN, fade_out_time=FADE_OUT, n=BLINKS, background=True) 
            time.sleep( SYNC_TIME_OFF )
            BIG_SWITCH_LED.blink(on_time=0, off_time=0, fade_in_time=0, fade_out_time=FADE_OUT, n=1, background=False)
            BIG_SWITCH_LED.off()
            print( "Gateway down.\n" )
        else:
            print( "Long press, bringing gateway up ..." )
            M2MG_LED.blink(on_time=BLINK_ON, off_time=BLINK_OFF, fade_in_time=FADE_IN, fade_out_time=FADE_OUT, n=BLINKS, background=True) 
            time.sleep( SYNC_TIME_ON )
            BIG_SWITCH_LED.blink(on_time=0, off_time=0, fade_in_time=FADE_IN, fade_out_time=0, n=1, background=False)
            BIG_SWITCH_LED.on()
            print( "Gateway up.\n" )
    else:
        print( "Short press. Doing nothing.\n" )
           



#main function
def main():
        m2mgState()
        # Set event for button press
        BIG_SWITCH.when_pressed = button

if __name__ == "__main__":
    main() 
             
            