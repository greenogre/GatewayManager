#!/usr/bin/python
# -*- coding: utf-8 -*-

## Thanks to furlongm for the code to get this started
## https://github.com/furlongm/openvpn-monitor/blob/master/openvpn-monitor.py

# Import libs
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

try:
    import ConfigParser as configparser
except ImportError:
    import configparser

import socket
import sys
import re
import argparse
from time import sleep

from datetime import datetime
#import time
# from pprint import pformat
# from collections import OrderedDict

# Define functions    
if sys.version_info[0] == 2:
    reload(sys)
    # sys.setdefaultencoding('utf-8')


def warning(*objs):
    print("WARNING: ", *objs, file=sys.stderr)


def debug(*objs):
    print("DEBUG:\n", *objs, file=sys.stderr)


def get_date(date_string, uts=False):
    if not uts:
        return datetime.strptime(date_string, "%a %b %d %H:%M:%S %Y")
    else:
        return datetime.fromtimestamp(float(date_string))

class ConfigLoader(object):

    def __init__(self, config_file):

        self.settings = {}
        #self. = OrderedDict()
        config = configparser.RawConfigParser()
        contents = config.read(config_file)

        if not contents:
            print('Config file does not exist or is unreadable, using defaults')
            self.load_default_settings()

        for section in config.sections():
            if section == 'ovpnManager':
                self.parse_global_section(config)
            else:
                print('Config file has missing section header or invalid format, using defaults.')
                self.load_default_settings()

    def load_default_settings(self):
        warning('Using default settings => /var/run/openvpn.socket')
        # self.settings = {'ovpnServer': 'Default Server'}
        self.settings = {'host': '/var/run/openvpn.socket',
                         'port': '7505', 'password': ''}

    def parse_global_section(self, config):
        global_vars = ['host', 'port', 'password' ]
        for var in global_vars:
            try:
                self.settings[var] = config.get('ovpnManager', var)
            except configparser.NoOptionError:
                pass
        if args.debug:
            debug("=== begin section\n{0!s}\n=== end section".format(self.settings))


class OvpnManager(object):

    def __init__(self, ovpn):
        self.ovpn = ovpn
       
    def status(self):
        self.command(self.ovpn,'status')
        if self.ovpn['last_cmd'] == 0:
            self.command(self.ovpn,'hold')
        return self.ovpn
    
    def up(self):
        self.command(self.ovpn,'up')
        return self.ovpn
        
    def down(self):
        self.command(self.ovpn,'down')
        return self.ovpn
    
    def hold(self):
        self.command(self.ovpn,'hold')
        return self.ovpn
    
    def hold_on(self):
        self.command(self.ovpn,'hold_on')
        return self.ovpn
    
    def hold_off(self):
        self.command(self.ovpn,'hold_off')
        return self.ovpn
    
    def command(self, ovpn, cmnd):
        self.ovpn = ovpn
        err_msg = ''
        self._socket_connect(ovpn)
        if self.s:
            login = self.authenticate(ovpn)
            if login[0]:
                if cmnd.lower() == 'status':
                    state = self.send_command('state\n')
                    ovpn['last_cmd'] = self.parse_state(state)[0]
                    ovpn['state'] = self.parse_state(state)[1]
                elif cmnd.lower() == 'up':
                    state = self.send_command('hold release\n')
                    ovpn['last_cmd'] = self.parse_state(state)[0]
                    ovpn['state'] = '2up'
                elif cmnd.lower() == 'down':
                    state = self.send_command('hold\n')
                    if self.parse_state(state)[1] == 1:
                        state = self.send_command('signal SIGUSR1\n')
                    else:
                        self.send_command('hold on\n')
                        state = self.send_command('signal SIGUSR1\n')
                        self.send_command('hold off\n')
                    ovpn['last_cmd'] = self.parse_state(state)[0]
                    ovpn['state'] = '2down'
                elif cmnd.lower() == 'reload':
                    state = self.send_command('signal SIGUSR1\n')
                    ovpn['last_cmd'] = self.parse_state(state)[0]
                    ovpn['state'] = 'reload'
                elif cmnd.lower() == 'hold':
                    state = self.send_command('hold\n')
                    ovpn['last_cmd'] = self.parse_state(state)[0]
                    ovpn['hold'] = self.parse_state(state)[1]
                elif cmnd.lower() == 'hold_on':
                    state = self.send_command('hold on\n')
                    ovpn['last_cmd'] = self.parse_state(state)[0]
                    ovpn['hold'] = self.parse_state(state)[1]
                elif cmnd.lower() == 'hold_off':
                    state = self.send_command('hold off\n')
                    ovpn['last_cmd'] = self.parse_state(state)[0]
                    ovpn['hold'] = self.parse_state(state)[1]
                else:
                    err_msg = "Invalid command: \"%s\"" % cmnd
            else:
                err_msg = login[1]
            self._socket_disconnect()
        else:
            err_msg = "No socket"
        ovpn['err_msg'] = err_msg

    
    def _socket_send(self, command):
        if sys.version_info[0] == 2:
            self.s.send(command)
        else:
            self.s.send(bytes(command, 'utf-8'))

    def _socket_recv(self, length):
        if sys.version_info[0] == 2:
            return self.s.recv(length)
        else:
            return self.s.recv(length).decode('utf-8')

    def _socket_connect(self, ovpn):
        host = ovpn['host']
        port = int(ovpn['port'])
        timeout = 3
        if re.match( '^/.*', host):
            retries = 0
            while retries <= 5:
                #print( "Socket connect try %d" % retries)
                s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
                s.settimeout(timeout)
                try:
                    s.connect(host)
                    self.s = s
                    ovpn['socket_connected'] = True
                    break
                except socket.timeout:
                    retries = retries + 1
                    waitTime = (retries * 1.5 + 1)
                    #print( "Socket retry number %d" % retries)
                    if retries >= 5:
                        self.s = False
                        ovpn['socket_connected'] = False
                        break
                    else:
                        sleep( waitTime )
                    
                except socket.error:
                    self.s = False
                    ovpn['socket_connected'] = False
        else:
            try:
                self.s = socket.create_connection((host, port), timeout)
                ovpn['socket_connected'] = True
            except socket.error:
                self.s = False
                ovpn['socket_connected'] = False

    def _socket_disconnect(self):
        self._socket_send('quit\n')
        self.s.close()

    def authenticate(self, ovpn):
        #print( "Authenticating ..." )
        err_msg = ''
        pw = ovpn[ 'password' ] + "\n"
        self._socket_send("\n")
        data = ''
        auth = False
        #print( pw )

        while 1:
            socket_data = self._socket_recv(1024)
            data += socket_data
            if data.endswith( "ENTER PASSWORD:" ):
                if pw != "\n":
                    data = ''
                    self._socket_send( pw )
                else:
                    err_msg = "Password required but no password set."
                    break
            else:
                auth = True
                break
        if args.debug:
            debug("=== begin raw data\n{0!s}\n=== end raw data".format(data))
        return auth, err_msg
        
        
    def send_command(self, command):
        self._socket_send(command)
        data = ''
        while 1:
            socket_data = self._socket_recv(1024)
            socket_data = re.sub('>INFO(.)*\r\n', '', socket_data)
            data += socket_data
            if command == '\n' and data != '':
                break
            elif data.endswith("\nEND\r\n"):
                break
            elif data.startswith('SUCCESS:'):
                break
            elif data.startswith('>HOLD:'):
                break
        if args.debug:
            debug("=== begin raw data\n{0!s}\n=== end raw data".format(data))
        return data
    
    @staticmethod
    def parse_state(data):
        state = {}
        for line in data.splitlines():
            result = 1
            parts = line.split(',')
            if args.debug:
                debug("=== begin split line\n{0!s}\n=== end split line".format(parts))
            if parts[0].startswith('>INFO') or \
               parts[0].startswith('END') or \
               parts[0].startswith('>CLIENT'):
                continue
            else:
                if parts[0].startswith('SUCCESS:'):
                    result = 0
                    bits = parts[0].split(':')
                    msg = bits[1].strip()
                    if msg.startswith('hold'):
                        if msg.startswith('hold=1') or msg.endswith('ON\n'):
                            state = 1
                        elif msg.startswith('hold=0') or msg.endswith('OFF\n'):
                            state = 0
                    break
                elif parts[0].startswith('>HOLD'):
                    result = 0
                    state = "HOLD"
                    break
                #state['up_since'] = get_date(date_string=parts[0], uts=True)
                elif parts[1] == 'CONNECTED':
                    result = 0
                    state = "UP"
                elif parts[1] == 'RECONNECTING':
                    result = 0
                    state = "DOWN"
                elif parts[1]:
                    result = 0
                    state = parts[1]
                break
        return result, state
    
def m2mg(cmnd='status'):
    global args
    args = collect_args().parse_args()
    cfg = ConfigLoader(args.config)
    m2mgMgr = OvpnManager(cfg.settings)
    # print( "M2mg Command: %s" % cmnd )
    if cmnd.lower() == 'status':
        m2mg_status = m2mgMgr.status()        
    elif cmnd.lower() == 'up':
        m2mg_status = m2mgMgr.up()
    elif cmnd.lower() == 'down':
        m2mg_status = m2mgMgr.down()
    elif cmnd.lower() == 'hold':
        m2mg_status = m2mgMgr.hold()
    elif cmnd.lower() == 'hold_on':
        m2mg_status = m2mgMgr.hold()
    elif cmnd.lower() == 'hold_off':
        m2mg_status = m2mgMgr.hold()
    else:
        m2mg_status = "Bad command: %s", cmnd
    
    #print( "M2mg Status: %s" % m2mg_status[ 'state' ] )
    return m2mg_status[ 'last_cmd' ], m2mg_status[ 'state' ]
        
#main function
def main():
    #print( "args.config:",args.config)
    cfg = ConfigLoader(args.config)
    monitor = OvpnManager(cfg.settings)
    #if args.debug:
    #    pretty_ovpn = pformat((dict(m2mg.ovpn)))
    #    debug("=== begin ovpn\n{0!s}\n=== end ovpn".format(pretty_ovpn))
    #return m2mg.ovpn
    print( "Init result:",monitor.ovpn)

def collect_args():
    parser = argparse.ArgumentParser(
        description='Get and set openvpn tunnel status')
    parser.add_argument('-d', '--debug', action='store_true',
                        required=False, default=False,
                        help='Run in debug mode')
    parser.add_argument('-c', '--config', type=str,
                        required=False, default='./ovpnMgr.conf',
                        help='Path to config file (./ovpnMgr.conf)')
    parser.add_argument('-C', '--cmnd', type=str,
                        required=False, default='status', 
                        help='Gateway command to execute')
    parser.add_argument('-H', '--host', type=str,
                        required=False, default='/var/run/openvpn.socket',
                        help='Name, IP address or path to socket for OVPN server to be managed')
    parser.add_argument('-p', '--port', type=str,
                        required=False, default='7505',
                        help='Port number of OVPN server to be managed (igonred if unix socket)')
    parser.add_argument('-P', '--password', type=str,
                        required=False, default='None',
                        help='Password for OVPN server management interface')
    return parser

if __name__ == "__main__":
    args = collect_args().parse_args()
    main() 
#else:
#    args = collect_args().parse_args() 
            